from django.shortcuts import render
from django.views.generic import TemplateView, ListView, UpdateView, CreateView, DeleteView, DetailView

# Create your views here.

class Home(TemplateView):

    template_name  = 'bases/home.html'